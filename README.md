#Domain fetcher

## Life cycle

1. Get domain.
2. Fetch all links (including source links) on a site.
3. Checking domains recursively until no new domain
    - Save checking time.
    - Get domain expiration date.
    - Save relations between domains.