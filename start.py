from lxml import html
from urllib.parse import urlparse
import requests
import database

url = 'http://swietateresa.pl' #in future move to frontend

#get only root domain
def get_root_domain(hostname):
    _h = hostname.split('.')
    return _h[len(_h) - 2] + '.' + _h[len(_h) - 1]

def check_domain(_url):
    templates = ['//a/@href', '//img/@src', '//link/@href', '//img/@src']
    blacklist = [':///', 'javascript:///', None]
    domains = []

    page = requests.get(_url)
    tree = html.fromstring(page.content)

    for template in templates:
        elements = tree.xpath(template)
        for element in elements:
            domain = urlparse(element).hostname
            if domain not in blacklist:
                domains.append(get_root_domain(domain))

    database.save(domains)

    return 1

check_domain(url)